CREATE TABLE `Colegio`.`Profesor` ( `Nif` VARCHAR(9) NOT NULL , `nom_profesor` VARCHAR(30) NOT NULL , `ap_profesor` VARCHAR(40) NOT NULL , `foto` VARCHAR(30) NOT NULL , PRIMARY KEY (`Nif`)) ENGINE = InnoDB;

CREATE TABLE `colegio`. ( `cod_curso` VARCHAR(2) NOT NULL , `nom_curso` VARCHAR(50) NOT NULL , PRIMARY KEY (`cod_curso`)) ENGINE = InnoDB;

CREATE TABLE `colegio`.`Alumno` ( `Nif` VARCHAR(9) NOT NULL , `nom_alumno` VARCHAR(30) NOT NULL , `ape_alumno` VARCHAR(40) NOT NULL , `foto` VARCHAR(30) NOT NULL , `cod_curso` VARCHAR(2) NOT NULL , PRIMARY KEY (`Nif`), INDEX (`cod_curso`)) ENGINE = InnoDB;

CREATE TABLE `colegio`.`Asignatura` ( `cod_asignatura` VARCHAR(4) NOT NULL , `nom_asignatura` VARCHAR(50) NOT NULL , `horas` INT NOT NULL , PRIMARY KEY (`cod_asignatura`)) ENGINE = InnoDB;

CREATE TABLE `colegio`.`CursoAsignatura` ( `cod_curso` VARCHAR(2) NOT NULL , `cod_asignatura` VARCHAR(4) NOT NULL , `nif_profesor` VARCHAR(9) NOT NULL , `aula` VARCHAR(4) NOT NULL , PRIMARY KEY (`cod_asignatura`, `nif_profesor`)) ENGINE = InnoDB;


Para hacer relaciones:
tabla -> estructura -> vista de relaciones ->relaciones interneas (NO, la QUE ESTA YA)

ALTER TABLE `alumno` ADD FOREIGN KEY (`cod_curso`) REFERENCES `curso`(`cod_curso`) ON DELETE NO ACTION ON UPDATE CASCADE;



ALTER TABLE `cursoasignatura` ADD FOREIGN KEY (`cod_curso`) REFERENCES `curso`(`cod_curso`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cursoasignatura` ADD FOREIGN KEY (`cod_asignatura`) REFERENCES `asignatura`(`cod_asignatura`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cursoasignatura` ADD FOREIGN KEY (`nif_profesor`) REFERENCES `profesor`(`Nif`) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE `colegio`.`Nota` ( `Nif_alumno` VARCHAR(9) NOT NULL , `cod_curso` VARCHAR(2) NOT NULL , `cod_asignatura` VARCHAR(4) NOT NULL , `calificacion` INT NOT NULL , PRIMARY KEY (`Nif_alumno`, `cod_curso`, `cod_asignatura`)) ENGINE = InnoDB;

ALTER TABLE `nota` ADD FOREIGN KEY (`Nif_alumno`) REFERENCES `alumno`(`Nif`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `nota` ADD FOREIGN KEY (`cod_curso`) REFERENCES `cursoasignatura`(`cod_curso`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `nota` DROP FOREIGN KEY `nota_ibfk_1`; ALTER TABLE `nota` ADD CONSTRAINT `nota_ibfk_1` FOREIGN KEY (`Nif_alumno`) REFERENCES `alumno`(`Nif`) ON DELETE CASCADE ON UPDATE CASCADE; ALTER TABLE `nota` DROP FOREIGN KEY `nota_ibfk_2`; ALTER TABLE `nota` ADD CONSTRAINT `nota_ibfk_2` FOREIGN KEY (`cod_curso`) REFERENCES `cursoasignatura`(`cod_curso`) ON DELETE NO ACTION ON UPDATE CASCADE; ALTER TABLE `nota` ADD FOREIGN KEY (`cod_asignatura`) REFERENCES `cursoasignatura`(`cod_asignatura`) ON DELETE NO ACTION ON UPDATE CASCADE;

(ha sido mas complkeja porque he puesto cascades y tal)

con esto se importa





INSERT INTO `curso` (`cod_curso`, `nom_curso`) VALUES ('00', 'NINGUNA'), ('1H', 'PRIMERO H'), ('2H', 'SEGUNDO H'), ('1J', 'PRIMERO J'), ('2J', 'SEGUNDO J')


Problemas de servidor, modificar estructura de la tabla
borra la tabla notas y cursoasignatura

cursoasignatura ahora va a tener
Id primary autoincrementativo
cod_asignatura varchar(4) index
cor_curso varchar(2) index
nif_profesor varchar(9) index
aula varchar(4)

asegurarse que los indices esten separados

luego las relaciones
curso -> codcurso cascades cascades
asignatura -> asignatura cascade cascades
profesor -> profesoee cascade cascade

NOTA

Id_asignatura int 
Nif_alumno varchar9
calificacion int

asignatura -> cursoAsignatura ID no action cascade
alumno -> alumno nif no action cascade


Ejercicio: insertar notas
